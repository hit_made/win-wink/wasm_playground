var mymod = require("../bin/hello-world-wasm.js");



(async () => {
    const hw = await mymod();
    console.log(hw.Greet.SayHello("jeff"));
})();